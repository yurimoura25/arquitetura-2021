package com.example.Java.Controller;

import java.util.List;

import com.example.Java.entity.Pessoa;
import com.example.Java.service.PessoaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("pessoa")
public class PessoaController {
    
    @Autowired
    PessoaService pessoaService;

    @GetMapping
    public ResponseEntity<?> buscar() {
        List<Pessoa> pessoas = pessoaService.buscar();
        return new ResponseEntity<>(pessoas, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> buscarPeloId(@PathVariable Long id) {
        Pessoa pessoa = pessoaService.buscarPeloId(id);
        return new ResponseEntity<>(pessoa, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> salvar(@RequestBody Pessoa pessoa) {
        Pessoa pessoaSalva = pessoaService.salvar(pessoa);
        return new ResponseEntity<>(pessoaSalva, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> atualizar(@RequestBody Pessoa pessoa) {
        pessoa = pessoaService.atualizar(pessoa);
        return new ResponseEntity<>(pessoa, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> atualizar(@PathVariable Long id) {
        boolean resposta = pessoaService.excluir(id);
        return new ResponseEntity<>(resposta, HttpStatus.OK);
    }

}
