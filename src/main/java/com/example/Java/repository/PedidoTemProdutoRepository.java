package com.example.Java.repository;

import com.example.Java.entity.PedidoTemProdutos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PedidoTemProdutoRepository extends JpaRepository<PedidoTemProdutos, Long> {
    
}
