package com.example.Java.service;

import java.util.List;

import com.example.Java.entity.Produto;
import com.example.Java.repository.ProdutoRepository;

import org.springframework.beans.factory.annotation.Autowired;

public class ProdutoService {
    

    @Autowired
    ProdutoRepository produtoRepository;

    
    public List <Produto> buscar() {
        return produtoRepository.findAll();
    }

    public Produto buscarPeloId(Long id) {
        return produtoRepository.getOne(id);
    }

    public Produto salvar(Produto Produto) {
        return produtoRepository.save(Produto);
    }

    public Produto atualizar(Produto Produto) {
        return produtoRepository.save(Produto);
    }

    public boolean excluir(Long id) {
        produtoRepository.deleteById(id);
        return true;
    }
}
