package com.example.Java.service;

import java.util.List;

import com.example.Java.entity.Categoria;
import com.example.Java.repository.CategoriaRepository;

import org.springframework.beans.factory.annotation.Autowired;

public class CategoriaService {
    

    @Autowired
    CategoriaRepository categoriaRepository;

    
    public List <Categoria> buscar() {
        return categoriaRepository.findAll();
    }

    public Categoria buscarPeloId(Long id) {
        return categoriaRepository.getOne(id);
    }

    public Categoria salvar(Categoria categoria) {
        return categoriaRepository.save(categoria);
    }

    public Categoria atualizar(Categoria categoria) {
        return categoriaRepository.save(categoria);
    }

    public boolean excluir(Long id) {
    categoriaRepository.deleteById(id);
        return true;
    }
}
