package com.example.Java.service;

import java.util.List;

import com.example.Java.entity.Pessoa;
import com.example.Java.repository.PessoaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PessoaService {

    //Injeção de dependência
    @Autowired
    PessoaRepository pessoaRepository;

    public List<Pessoa> buscar() {
        return pessoaRepository.findAll();
    }

    public Pessoa buscarPeloId(Long id) {
        return pessoaRepository.getOne(id);
    }

    public Pessoa salvar(Pessoa pessoa) {
        return pessoaRepository.save(pessoa);
    }

    public Pessoa atualizar(Pessoa pessoa) {
        return pessoaRepository.save(pessoa);
    }

    public boolean excluir(Long id) {
        pessoaRepository.deleteById(id);
        return true;
    }
}
