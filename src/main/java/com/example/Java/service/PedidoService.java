package com.example.Java.service;

import java.util.List;

import com.example.Java.entity.Pedido;
import com.example.Java.repository.PedidoRepository;

import org.springframework.beans.factory.annotation.Autowired;

public class PedidoService {
    
    @Autowired
    PedidoRepository pedidoRepository;

    
    public List<Pedido> buscar() {

        return pedidoRepository.findAll();
    }

    public Pedido buscarPeloId(Long id) {
        return pedidoRepository.getOne(id);
    }

    public Pedido salvar(Pedido Pedido) {
        return pedidoRepository.save(Pedido);
    }

    public Pedido atualizar(Pedido Pedido) {
        return pedidoRepository.save(Pedido);
    }

    public boolean excluir(Long id) {
        pedidoRepository.deleteById(id);
        return true;
    }
}
